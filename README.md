### I2C Registers

The writing address of this device is 0x60 and it is controlled by five bytes registers.

#### Write mode:

Bit | Symbol    | Description
--- | --------- | -----------
| **Byte 1**    ||
7   | MUTE      | if MUTE = 1 then L and R audio are muted
6   | SM        | Search mode: if SM = 1 then in search mode; if SM = 0 then not in search mode
5-0 | PPL[13:8] | setting of synthesizer programmable counter for search or preset
| **Byte 2**    ||
0-7 | PPL[7:0]  | etting of synthesizer programmable counter for search or preset
| **Byte 3**    ||
7   | SUD       | Search Up/Down: if SUD = 1 then search up; if SUD = 0 then search down
6-5 | SSL[1:0]  | Search Stop Level: [0-1] = low, [1-0] = mid, [1-1] = high
4   | HLSI      | High/Low Side Injection: if HLSI = 1 then high side LO injection; if HLSI = 0 then low side LO injection
3   | MS        | Mono to Stereo: if MS = 1 then forced mono; if MS = 0 then stereo ON
2   | MR        | Mute Right: if MR = 1 then the right audio channel is muted and forced mono
1   | ML        | Mute Left:if ML = 1 then the left audio channel is muted and forced mono
0   | SWP1      | -
| **Byte 4**    ||
7   | SWP2      | -
6   | STBY      | Standby: if STBY = 1 then in Standby mode; if STBY = 0 then not in Standby mode
5   | BL        | Band Limits: if BL = 1 then Japanese FM band; if BL = 0 then US/Europe FM band
4   | XTAL      | Clock frequency: Reg PLLREF=0 and XTAL=1 clocl freq=32.768KHz
3   | SMUTE     | Soft Mute: if SMUTE = 1 then soft mute is ON
2   | HCC       | High Cut Control: if HCC = 1 then high cut control is ON
1   | SNC       | Stereo Noise Cancelling:if SNC = 1 then stereo noise cancelling is ON
0   | SI        | -
| **Byte 5**    ||
7   | PLLREF    | see the reg XTAL
6   | DTC       | if DTC = 1 then the de-emphasis time constant is 75µs; if DTC = 0 then the de-emphasis time constant is 50µs
5-0 | -         | -

#### Read mode

Bit | Symbol    | Description
--- | --------- | -----------
| **Byte 1**    ||
7   | RF        | Ready Flag: if RF = 1 then a station has been found or the band limit has been reached
6   | BLF       | Band Limit Flag: if BLF = 1 then the band limit has been reached
5-0 | PLL[13:8] | setting of synthesizer programmable counter for search or preset
| **Byte 2**    ||
7-0 | PLL[7:0]  | setting of synthesizer programmable counter for search or preset
| **Byte 3**    ||
7   | STEREO    | Stereo indication:if STEREO = 1 then stereo reception; if STEREO = 0 then mono reception
6-0 | IF[6:0]   | IF counter result
| **Byte 4**    ||
7-4 | LEV[3:0]  | level ADC output
3-1 | CI[2:0]   |   Chip Identification: these bits have to be set to logic 0
0   | -         | -

## Frequency Tuning

http://en.wikipedia.org/wiki/Superheterodyne_receiver

The Frequency is calculated by the following formula that return a 14bits word:

```
// High side injection
N = (4 * (Frf + Fif)) / Fref

// Low side injection
N = (4 * (Frf - Fif)) / Fref

N    = decimal value of PPL word
Frf  = wanted tuning frequency [Hz]
Fif  = the intermediate frequency [Hz] = 225kHz
Fref = the reference frequency [Hz] = 32.768kHz for 32.768kHz crystal;
```
