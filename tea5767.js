const { Bus, Device } = require('async-i2c-bus');

const COMMAND_LENGTH = 5;

const open = async () => {
  const bus = Bus(1);
  await bus.open();
  const device = Device({ address: 0x60, bus });
  return device;
};

const read = async () => {
  const device = await open();
  const buffer = Buffer.alloc(COMMAND_LENGTH);
  try {
    await device.i2cRead(COMMAND_LENGTH, buffer);
  } catch (error) {
    if (process.arch !== 'x64') {
      throw error;
    }
  }
  await device.bus.close();
  return new Uint8Array(buffer);
};

const write = async (command) => {
  const device = await open();
  try {
    await device.i2cWrite(COMMAND_LENGTH, Buffer.from(command));
  } catch (error) {
    if (process.arch !== 'x64') {
      throw error;
    }
  }
  await device.bus.close();
};

const getFrequency = (frequency) => {
  const frequencyB = (4 * ((frequency * 1000000) + 225000)) / 32768;
  const frequencyH = frequencyB >> 8;
  const frequencyL = frequencyB & 0XFF;
  return { frequencyH, frequencyL };
};

module.exports = {
  setFrequency: async (frequency) => {
    const { frequencyH, frequencyL } = getFrequency(frequency);
    const command = new Uint8Array([
      frequencyH,
      frequencyL,
      0xB0,       // high side LO injection is on
      0x10,       // Xtal is 32.768 kHz
      0x00        // not used
    ]);
    await write(command);
  },

  mute: async () => {
    const { frequencyH, frequencyL } = getFrequency(0);
    const command = new Uint8Array([
      frequencyH | 0x80,
      frequencyL,
      0xB0,       // high side LO injection is on
      0x10,       // Xtal is 32.768 kHz
      0x00        // not used
    ]);
    await write(command);
  },

  standby: () => {},

  getStatus: async () => {
    const data = await read();

    const ready = !!(data[0] & 0x80);
    const stereo = !!(data[2] & 0x80);
    const signalLevel = ((data[3] & 0xf0) >> 4);

    let frequency;
    frequency = ((((((data[0] & 0x3F) << 8) + data[1]) * 32768) / 4) - 225000) / 1000000;
    frequency = Math.round(frequency * 10) / 10;

    return {
      frequency,
      ready,
      stereo,
      signalLevel
    };
  }
};

