#!/usr/bin/env node
const { getStatus } = require('./tea5767');

getStatus().then((status) => {
    process.stdout.write(`${JSON.stringify(status, null, 4)}\n`);
});
